import logo from './logo.svg';
import './App.css';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getInfectionData, setStartingDate } from './redux/actions';
import { getInfectionData as GETInfected } from './redux/selectors';
import { getStartingDate } from './redux/selectors';
import { getEndingDate } from './redux/selectors';
import { Chart } from './stories/Chart';
import DatePicker from 'react-date-picker';

function App() {
  const startingDate = useSelector(getStartingDate());
  const endingDate = useSelector(getEndingDate());

  const infected = useSelector(GETInfected(Date.parse(startingDate) - 50000000000, Date.parse(endingDate)));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getInfectionData());
  }, [dispatch]);

  console.log(infected[0].infected);
  
  console.log(startingDate)

  const [value, onChange] = useState(new Date());
  return (
    <div className='Covid-Tracker'>
      <DatePicker
        onChange={onChange}
        value={value}
      />
    <Chart type = "monotone" width = {1600} height = {900} chartData = {infected}></Chart>
    </div>

  );
}

export default App;
