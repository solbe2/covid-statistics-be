import { Action } from "redux";
import { Store, initStore, InfectionData } from "./store";

export interface IGetInfected extends Action<"GETINFECTED"> {
  payload: {
    infected: number;
  };
}

export interface IGetInfectionData extends Action<"GETINFECTIONDATA"> {
  payload: {
    infectionData: InfectionData[];
  };
}

export interface IGetActiveInfected extends Action<"GETACTIVEINFECTED"> {
  payload: {
    activeInfected: number;
  };
}

export interface IGetQuarantined extends Action<"GETQUARANTINED"> {
  payload: {
    quarantined: number;
  };
}

export interface ISetStartingDate extends Action<"STARTINGDATE"> {
  payload: {
    startingDate: string;
  };
}

export interface ISetEndingDate extends Action<"ENDINGDATE"> {
  payload: {
    endingDate: string;
  };
}

const getInfectedReducer = (store: Store, { payload: { infected } }: IGetInfected ): Store => {
  return {
    ...store,
    infected
  };
};

const getActiveInfectedReducer = (store: Store, { payload: { activeInfected } }: IGetActiveInfected ): Store => {
  return {
    ...store,
    activeInfected
  };
};

const getQuarantinedReducer = (store: Store, { payload: { quarantined } }: IGetQuarantined ): Store => {
  return {
    ...store,
    quarantined
  };
};

const getInfectionData = (store: Store, { payload: { infectionData } }: IGetInfectionData ): Store => {
  return {
    ...store,
    infectionData
  };
};

const setStartingDateReducer = (store: Store, { payload: { startingDate } }: ISetStartingDate ): Store => {
  store.startingDate = startingDate;
  return {
    ...store,
    startingDate
  };
};

const setEndingDateReducer = (store: Store, { payload: { endingDate } }: ISetEndingDate ): Store => {
  store.endingDate = endingDate;
  return {
    ...store,
    endingDate
  };
};

export const reducer = (store: Store = initStore, action: Action) => {
  switch (action.type) {
    case "GETINFECTED":
      return getInfectedReducer(store, action as IGetInfected);
    case "GETACTIVEINFECTED":
      return getActiveInfectedReducer(store, action as IGetActiveInfected);
    case "GETQUARANTINED":
      return getQuarantinedReducer(store, action as IGetQuarantined);
    case "GETINFECTIONDATA":
      return getInfectionData(store, action as IGetInfectionData);
    case "STARTINGDATE":
        return setStartingDateReducer(store, action as ISetStartingDate);
    case "ENDINGDATE":
        return setEndingDateReducer(store, action as ISetEndingDate);

    default:
      return store;
  }
};