import { fetchInformation } from "../fetch-api/requests";
import { IGetInfectionData, ISetEndingDate, ISetStartingDate } from "./reducer";

export const getInfectionData = async (): Promise<IGetInfectionData> => {
    try {
      return {
        type: "GETINFECTIONDATA",
        payload: {
          infectionData: (await fetchInformation())
        }
      };
    } catch (error) {
      return {
        type: "GETINFECTIONDATA",
        payload: {
          infectionData: []
        }
      };
    }
  };
  

  export const setStartingDate = (startingDate: string): ISetStartingDate => ({
    type: "STARTINGDATE",
    payload: {
      startingDate
    }
  });

  export const setEndingDate = (endingDate: string): ISetEndingDate => ({
    type: "ENDINGDATE",
    payload: {
      endingDate
    }
  });