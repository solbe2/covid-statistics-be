export const middleware = () => (next: any) => (action: any) => {
    if (action instanceof Promise) {
      return action.then((data) => next(data));
    }
    next(action);
};
  