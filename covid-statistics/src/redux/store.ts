import { createStore, applyMiddleware } from "redux";
import { reducer } from "./reducer";
import { middleware } from "./middleware";

export interface InfectionData {
  infected: number,
  deceased: number
  recovered: number,
  quarantined: number,
  tested: number,
  sourceUrl: string,
  lastUpdatedAtApify: string,
  readMe: string
}

export const initStore = {
  infected: 0,
  activeInfected: 0,
  quarantined: 0,
  startingDate: "2020-03-15T11:46:00.000Z",
  endingDate: "2021-06-08T11:46:00.000Z",
  infectionData: [{infected: 0, deceased: 0, recovered: 0, quarantined: 0, tested: 0, sourceUrl: "", lastUpdatedAtApify: "2020-03-15T11:46:00.000Z", readMe: ""}] as InfectionData[]
};

export type Store = typeof initStore;
export const store = createStore(reducer, applyMiddleware(middleware));

function applyToStore(callback: any, store: typeof initStore) {
  return callback(store);
}