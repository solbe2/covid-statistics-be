import { initStore as Store } from "./store";

export const getInfectionData = (StartingDate: number, EndingDate: number) => (store: typeof Store) => store.infectionData.filter(Data => Date.parse(Data.lastUpdatedAtApify) > StartingDate && Date.parse(Data.lastUpdatedAtApify) < EndingDate);

export const getInfected = () => (store: typeof Store) => store.infected;

export const getActiveInfected = () => (store: typeof Store) => store.activeInfected;

export const getQuarantined = () => (store: typeof Store) => store.quarantined;

export const getStartingDate = () => (store: typeof Store) => store.startingDate;

export const getEndingDate = () => (store: typeof Store) => store.endingDate;
