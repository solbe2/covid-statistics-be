import React from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';

interface ChartData {
  infected: number,
  deceased: number
  recovered: number,
  quarantined: number,
  tested: number,
  sourceUrl: string,
  lastUpdatedAtApify: string,
  readMe: string
}

export interface ChartProps {
    type: "monotone",
    width: number,
    height: number,
    chartData: ChartData[]
}

/**
 * Primary UI component for user interaction
 */
export const Chart: React.FC<ChartProps> = ({
  type,
  width,
  height,
  chartData,
  ...props
}) => {

  let data = [];

  for(var i = 0; i < chartData.length; i++){
    data.push({name: chartData[i].lastUpdatedAtApify, Infected: chartData[i].infected, Recovered: chartData[i].recovered, Quarantined: chartData[i].quarantined, amt: 2400})
  }

  return (
    <LineChart width={width} height={height} data={data} margin={{
      top: 5,
      right: 30,
      left: 20,
      bottom: 5,
    }}>
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis dataKey="name" />
    <YAxis />
    <Tooltip />
    <Legend />
    <Line type="monotone" dataKey="Infected" stroke="#8884d8" activeDot={{ r: 8 }}/>
    <Line type="monotone" dataKey="Recovered" stroke="#82ca9d" />
    <Line type="monotone" dataKey="Quarantined" stroke="#82119d" />
    </LineChart>
  );
};
