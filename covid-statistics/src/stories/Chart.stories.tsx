import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Chart, ChartProps } from './Chart';

interface ChartData {
  infected: number,
  deceased: number
  recovered: number,
  quarantined: number,
  tested: number,
  sourceUrl: string,
  lastUpdatedAtApify: string,
  readMe: string
}

export default {
  title: 'Chart',
  component: Chart,
} as Meta;

const Template: Story<ChartProps> = (args) => <Chart {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  type: "monotone",
  width: 400,
  height: 400,
  chartData: [{lastUpdatedAtApify:"első", infected: 10, recovered: 20, quarantined:  5},{lastUpdatedAtApify:"második", infected: 20, recovered: 10, quarantined:  5}] as ChartData[]
};