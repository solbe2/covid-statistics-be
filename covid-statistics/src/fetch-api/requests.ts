const header = <H extends {}>(header: H = {} as H) => ({
    "Content-type": "application/json",
    ...header
});
  
const fetchRequest = async <Res>(method: string, url: string) => {
    const res = fetch(url, {
      method,
      headers: header(),
      cache: "no-cache"
    });
    return (await res).json() as Promise<Res>;
};
  
export const fetchInformation = () =>
    fetchRequest<[{
      infected: number,
      deceased: number
      recovered: number,
      quarantined: number,
      tested: number,
      sourceUrl: string,
      lastUpdatedAtApify: string,
      readMe: string
//}>("GET", "https://api.apify.com/v2/key-value-stores/RGEUeKe60NjU16Edo/records/LATEST?disableRedirect=true?token=4sn38aTqdvLb9gCZfjy2Gwudj");
}]>("GET", "https://api.apify.com/v2/datasets/Gm6qjTgGqxkEZTkuJ/items?format=json&clean=1&token=4sn38aTqdvLb9gCZfjy2Gwudj");